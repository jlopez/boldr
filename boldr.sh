#!/bin/bash

# Exists on error or use of unused variable
set -euo pipefail

# Empty match -> empty array
shopt -s nullglob

## User variables
ROOTDIR="$HOME/eclipse/workspace/phd"
HBASELIBDIR="$HOME/hbase/hbase-1.2.6/lib"
HIVELIBDIR="/usr/local/hive/lib"
GITLABUSERNAME="jlopez"

## Internal variables (don't touch them!)
COMMANDS=""
REMOTEROOT="https://www.lri.fr/~lopez"
NEVER_FAIL="false"
DEVELOPMENT="false"

MX="$ROOTDIR/mx/mx"
GIT="git"
WGET="wget"
TAR="tar"
ANT="ant"
CP="cp -r"
RM="rm -f"

function check_qir
{
  if ! test -f "$ROOTDIR/qir/qir.jar"; then
    cd "$ROOTDIR/qir"
    test -d src_gen || mkdir src_gen
    $ANT
    cd - > /dev/null
  fi
}

function install_deps
{
  test -d lib || mkdir lib || exit $?
  test -f lib/postgresql.jar || $WGET -q -O lib/postgresql.jar "https://jdbc.postgresql.org/download/postgresql-42.1.1.jar"
  test -f lib/ojdbc.jar || ($WGET -q "http://www.java2s.com/Code/JarDownload/ojdbc14/ojdbc14.jar.zip" && unzip -qq ojdbc14.jar.zip && rm ojdbc14.jar.zip && mv ojdbc14.jar lib/ojdbc.jar)
  test -d mx || $GIT clone https://github.com/graalvm/mx
  test -d jvmci || $GIT clone https://github.com/graalvm/graal-jvmci-8 jvmci
  cd jvmci
  $MX build
  cd - > /dev/null
  test -d qir || $GIT clone https://$GITLABUSERNAME@gitlri.lri.fr/jlopez/qir.git
}

function install
{
  install_deps
  if ! test -d "$2"; then
    $GIT clone "https://$GITLABUSERNAME@gitlri.lri.fr/jlopez/$1.git" "$2"
    check_qir
    cd "$3"
    $MX build || printf "" > /dev/null # TODO: build should pass
    if test "$DEVELOPMENT" = "true"; then
      $GIT remote add "$2" "https://github.com/oracle/$2"
      $GIT checkout -b $2
      $GIT fetch --all
      $GIT branch --set-upstream-to="$2/master"
      $GIT checkout master
    fi
    cd - > /dev/null
  fi
}

function install_qsl
{
  install "qsl" "graal" "$ROOTDIR/graal/truffle/src/com.oracle.truffle.sl"
}

function install_r
{
  install "QueryR" "fastr" "$ROOTDIR/fastr"
}

function install_all
{
  install_qsl
  install_r
}

function update
{
  for i in jvmci graal mx qir fastr; do
    test -d $i && $GIT -C $i pull
  done
}

function clean
{
  test -d jvmci && cd jvmci && $MX clean && cd - > /dev/null
  for i in graal/truffle fastr; do
    test -d $i && cd $i && $MX clean && cd - > /dev/null
  done
}

function eclipse
{
  QIRBUILT="yes"
  QIRJAR="graal/truffle/src/com.oracle.truffle.sl"
  OTHERJAR="graal/truffle/src/com.oracle.truffle.sl
  qir"
  test -f "$ROOTDIR/qir/qir.jar" || QIRBUILT="no"
  test $QIRBUILT = "yes" || touch "$ROOTDIR/qir/qir.jar"
  if test -d "$ROOTDIR/jvmci"; then
      cd "$ROOTDIR/jvmci" || exit $?
      $MX build
      JAVA_VERSION="$(javac -version 2>&1)"
      if test $? -ne 0; then
	  printf "No java found in path"
	  exit 1
      fi
      $MX eclipseinit
  fi
  if test -d "$ROOTDIR/graal/compiler"; then
      cd "$ROOTDIR/graal/compiler" || exit $?
      JAVA_HOME="$ROOTDIR/jvmci/jdk$(echo $JAVA_VERSION | cut -d ' ' -f 2)/linux-amd64/product" $MX eclipseinit
  fi
  if test -d "$ROOTDIR/fastr"; then
    cd "$ROOTDIR/fastr"
    RESULT=0
    $MX eclipseinit || RESULT=$?
    if test $RESULT -eq 0; then
      QIRJAR="$QIRJAR fastr/com.oracle.truffle.r.engine
      fastr/com.oracle.truffle.r.nodes
      fastr/com.oracle.truffle.r.nodes.builtin
      fastr/com.oracle.truffle.r.runtime"
      OTHERJAR="$OTHERJAR fastr/com.oracle.truffle.r.engine"
    fi
  fi
  $WGET -q "$REMOTEROOT/phd/phd.tar.gz" || exit $?
  $TAR -xf phd.tar.gz
  $RM phd.tar.gz
  $CP phd/qir/.[a-z]* "$ROOTDIR/qir" || exit $?
  $RM -r phd
  for i in $QIRJAR; do
    grep -sq qir "$ROOTDIR/$i/.classpath" || sed -i 's,</classpath>,\t<classpathentry kind="lib" path="'"$ROOTDIR"'/qir/qir.jar"/>\n</classpath>,' "$ROOTDIR/$i/.classpath"
  done
  for i in $OTHERJAR; do
    grep -sq ojdbc "$ROOTDIR/$i/.classpath" || sed -i 's,</classpath>,\t<classpathentry kind="lib" path="'"$ROOTDIR"'/lib/ojdbc.jar"/>\n</classpath>,' "$ROOTDIR/$i/.classpath"
    grep -sq postgresql "$ROOTDIR/$i/.classpath" || sed -i 's,</classpath>,\t<classpathentry kind="lib" path="'"$ROOTDIR"'/lib/postgresql.jar"/>\n</classpath>,' "$ROOTDIR/$i/.classpath"
    for jar in $HBASELIBDIR/*.jar $HIVELIBDIR/*.jar; do
      if test $(basename "$jar") != "R.jar" && test $(basename "$jar") != "sl.jar"; then
        grep -sq "$jar" "$ROOTDIR/$i/.classpath" || sed -i 's,</classpath>,\t<classpathentry kind="lib" path="'"$jar"'"/>\n</classpath>,' "$ROOTDIR/$i/.classpath"
      fi
    done
  done
  test $QIRBUILT = "yes" || $RM "$ROOTDIR/qir/qir.jar"
}

function usage
{
  printf "Usage: $0 (option|command)*\n"
  printf "The commands are run sequentially until one fail.\n"
  printf "Valid options:\n"
  printf "  -h, --help: Print this usage and exit.\n"
  printf "  -nf, --never-fail: Run all given commands even if one fails.\n"
  printf "  -d, -dev, --development: Install the development version.\n"
  printf "Valid commands:\n"
  printf "  install_qsl: Install QSL.\n"
  printf "  install_r: Install R.\n"
  printf "  install_all, install: (default) Install all languages, equivalent to all other\n"
  printf "    install commands run together.\n"
  printf "  update: Update boldr and its dependencies.\n"
  printf "  clean: Applies the cleaning suite for every component of boldr.\n"
  printf "  eclipse: Allow boldr languages to be run with eclipse (see README).\n"
  exit $1
}

function parse_options
{
  while test $# -ne 0; do
    test $1 = "--help" && usage 0
    test $1 = "-h" && usage 0
    if test $1 = "-nf" || test $1 = "--never-fail"; then
      NEVER_FAIL="true"
    elif test $1 = "-d" || test $1 = "-dev" || test $1 = "--development"; then
      DEVELOPMENT="true"
    elif test `printf '$1' | cut -c 1` = "-"; then
      printf "Invalid option: $1.\n"
      usage 1
    else
      COMMANDS="$COMMANDS $1"
    fi
    shift
  done
}

function main
{
  parse_options $@
  # Create and enter safe environment
  test -d "$ROOTDIR" || mkdir "$ROOTDIR" || exit $?
  cd $ROOTDIR

  # Run the commands
  test "$COMMANDS" = "" && install_all
  for COMMAND in $COMMANDS; do
    if test "$COMMAND" = "install_qsl"; then
      install_qsl
    elif test "$COMMAND" = "install_r"; then
      install_r
    elif test "$COMMAND" = "install_all" || test "$COMMAND" = "install"; then
      install_all
    elif test "$COMMAND" = "update"; then
      update
    elif test "$COMMAND" = "clean"; then
      clean
    elif test "$COMMAND" = "eclipse"; then
      eclipse
    else
      printf "Invalid command: $COMMAND\n"
      usage 1
    fi
    RETCODE=$?
    test "$NEVER_FAIL" = "true" || test "$RETCODE" -eq 0 || exit "$RETCODE"
  done
}

main $@
